﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Inscyth.Spectrum.Azure
{
    public interface IAzureStorageService
    {
        Task<CloudBlockBlob> UploadFile(IFormFile file, string blobFolderName);
    }

    public class AzureStorageService : IAzureStorageService
    {
        private ILogger<AzureStorageService> _logger;
        private readonly CloudBlobContainer _blobContainer;
        private readonly AzureStorageConfig _storageConfig;

        public AzureStorageService(ILogger<AzureStorageService> logger, IOptions<AzureStorageConfig> config)
        {
            _logger = logger;
            _storageConfig = config.Value;
            var storageAccount = CloudStorageAccount.Parse(_storageConfig.ConnectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();
            _blobContainer = blobClient.GetContainerReference(_storageConfig.ContainerName);
            
            _blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Container });
            _blobContainer.CreateIfNotExistsAsync().Wait();
        }

        public async Task<CloudBlockBlob> UploadFile(IFormFile file, string blobFolderName)
        {
            try
            {
                blobFolderName = blobFolderName + "/" + file.FileName;
                var cloudBlock = _blobContainer.GetBlockBlobReference(blobFolderName);
                cloudBlock.Properties.ContentType = GetContentType(file.FileName);

                using (var fileStream = file.OpenReadStream())
                {
                    await cloudBlock.UploadFromStreamAsync(fileStream);
                }

                return cloudBlock;
            }
            catch (Exception ex)
            {
                _logger.LogError("Azure Storage Error. Exception: " + ex.Message);
                return null;
            }
        }

        private string GetContentType(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(fileName, out var contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }
    }

    public class AzureStorageConfig
    {
        public string ConnectionString { get; set; }
        public string ContainerName { get; set; }
        public string AccountKey { get; set; }
    }
}