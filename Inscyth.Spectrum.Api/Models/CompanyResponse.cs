﻿using System;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class CompanyResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CompanyWebsite { get; set; }
        public string ContactName { get; set; }
        public CompanyType CompanyType { get; set; }
        public UserResponse CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class UserResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }

        public UserType UserType { get; set; }
        public Role Role { get; set; }

        public int? CompanyId { get; set; }
        public string Company { get; set; }
        public int Id { get; set; }
    }
}