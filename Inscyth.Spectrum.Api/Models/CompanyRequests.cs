﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class CompanyCreateRequest
    {
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CompanyWebsite { get; set; }
        public string ContactName { get; set; }
        public int? PrimaryContactId { get; set; }
        
        public CompanyCreateRequest(string name)
        {
            Name = name;
        }

        public CompanyCreateRequest()
        {
            
        }
    }
    public class CompanyUpdateRequest
    {
        public string? Name { get; set; }
        public CompanyType? CompanyType { get; set; }
        public string? Address { get; set; }
        public string? Address2 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? ZipCode { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? ContactName { get; set; }
        public string? CompanyWebsite { get; set; }
        public int? PrimaryContactId { get; set; }

    }
}
