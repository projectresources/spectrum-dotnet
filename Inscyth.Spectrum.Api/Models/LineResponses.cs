﻿using System;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class LineResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LineType { get; set; }

        public double? TotalMiles { get; set; }
        public double? TotalFeet { get; set; }
     
        public string? StartDescription { get; set; }
        public string? EndDescription { get; set; }   
        
        public int AreaId { get; set; }
        public AreaResponse Area { get; set; }
        public FileViewResponse[] Files { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
    }
} 