﻿using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class UserRegistrationRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int? CompanyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public UserType UserType { get; set; }
        public Role Role { get; set; }
    }

    public class UpdateUserRequest
    {
        public string Email { get; set; }
        public string? Password { get; set; }
        public string? CurrentPassword { get; set; }
        public string UserName { get; set; }
        public int? CompanyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public UserType UserType { get; set; }
        public Role Role { get; set; }
    }
}