﻿using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class AreaResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string? LocStartDescription { get; set; }
        public string? LocEndDescription { get; set; }

        public double? LocationStart { get; set; }
        public double? LocationEnd { get; set; }
        public RegionResponse Region { get; set; }
        public int RegionId { get; set; }
        public int CompanyId { get; set; }
        public LineResponse[]? Lines { get; set; }
    }
    
}