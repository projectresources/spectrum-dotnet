﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inscyth.Spectrum.Models
{
    public class UserAuditResponse
    {
        public int Id {get;set;}
        public string UserId { get; private set; }

        public DateTimeOffset Timestamp { get; private set; }

        public int AuditEvent { get; set; }
        
        public string Name { get; set; }

        public string IpAddress { get; private set; }
        public DateTime CreatedAt {get;set;}
    }
}
