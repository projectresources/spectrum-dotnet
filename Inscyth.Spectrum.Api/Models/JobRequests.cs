﻿using System;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class JobCreateRequest
    {
        public string? Description { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public int PurchaseOrderId { get; set; }
        
        public JobStatus JobStatus { get; set; }
        public JobType JobType { get; set; }
    }
}