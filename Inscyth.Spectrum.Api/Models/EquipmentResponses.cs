﻿using System;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class EquipmentResponse
    {
        public int Id { get; set; }
        public EquipmentType Type { get; set; }
        public string TypeText { get; set; }
        public string Name { get; set; }
        public string? SerialNumber { get; set; }
        public string? Description { get; set; }
        public string? Instructions { get; set; }
        public string? SoftwareReferenceLink { get; set; }
        public DateTime? LastServiceDate { get; set; }
        public bool IsOutOfService { get; set; }
    }
}