﻿using System;

namespace Inscyth.Spectrum.Models
{
    public class VehicleResponse
    {
        public int Id { get; set; }
        public string? Vin { get; set; }
        public string? Notes { get; set; }
        public int DoeId { get; set; }
        public int VehicleYear { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}