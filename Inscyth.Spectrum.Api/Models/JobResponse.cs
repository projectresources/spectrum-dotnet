﻿using System;
using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class JobResponse
    { 
        public int Id { get; set; }
        public string? Description { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public int PurchaseOrderId { get; set; }
        public PurchaseOrderResponse PurchaseOrder { get; set; }
        
        public JobStatus JobStatus { get; set; }
        public JobType JobType { get; set; }
        
        public string JobStatusText { get; set; }
        public string JobTypeText { get; set; }
    }
}