﻿namespace Inscyth.Spectrum.Models
{
    public class PurchaseOrderCreateRequest
    {
        public string Name { get; set; }
        
        public int CompanyId { get; set; }
    }
}