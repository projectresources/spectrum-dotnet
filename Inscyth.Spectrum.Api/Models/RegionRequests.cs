﻿namespace Inscyth.Spectrum.Models
{
    public class RegionCreateRequest
    {
        public string Name { get; set; }

        public string? LocStartDescription { get; set; }
        public string? LocEndDescription { get; set; }
        
        public double? LocationStart { get; set; }
        public double? LocationEnd { get; set; }
        
        public int CompanyId { get; set; }
    }
}