﻿using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class LineCreateRequest
    {
        public string Name { get; set; }
        
        public double? TotalMiles { get; set; }
        public double? TotalFeet { get; set; }
     
        public string? StartDescription { get; set; }
        public string? EndDescription { get; set; }
        
        public string LineType { get; set; }
        
        public int AreaId { get; set; }
        
    }
}