﻿namespace Inscyth.Spectrum.Models
{
    public class FileViewResponse
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public int LineId { get; set; }
    }
}