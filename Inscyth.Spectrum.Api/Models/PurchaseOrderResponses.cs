﻿namespace Inscyth.Spectrum.Models
{
    public class PurchaseOrderResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public string Company { get; set; }
        public int[] JobIds { get; set; }
    }
}