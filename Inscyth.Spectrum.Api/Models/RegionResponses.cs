﻿using Inscyth.Spectrum.Data.Entities;

namespace Inscyth.Spectrum.Models
{
    public class RegionResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string LocStartDescription { get; set; }
        public string LocEndDescription { get; set; }
        
        public double? LocationStart { get; set; }
        public double? LocationEnd { get; set; }
        
        public int CompanyId { get; set; }
        public string Company { get; set; }
        public AreaResponse[] Areas { get; set; }
        
    }
}