﻿using Microsoft.AspNetCore.Http;

namespace Inscyth.Spectrum.Models
{
    public class FileRequest
    {
        public int LineId { get; set; }

        public IFormFile File { get; set; }
        public FileTypeEnum? FileType { get; set; }
    
        public FileRequest(IFormFile file, FileTypeEnum fileType)
        {
            File = file;
            FileType = fileType;
        }

        public FileRequest()
        {
            
        }
    }

    public enum FileTypeEnum
    {
        New,
        Pending,
        InProcess,
        Approved,
        Closed
    }
}