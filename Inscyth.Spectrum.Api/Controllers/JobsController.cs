﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Extensions;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class JobsController : ControllerBase
    {
        private IJobService _jobService;
        private IMapper _mapper;

        public JobsController(IJobService jobService, IMapper mapper)
        {
            _jobService = jobService;
            _mapper = mapper;
        }
        
        // GET: api/Jobs
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var entities = await _jobService.GetAllListAsync();
            var response = _mapper.Map<List<Job>, List<JobResponse>>(entities);
            return Ok(response);
        }

        // GET: api/Jobs/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<JobResponse>> GetById(int id)
        {
            var entity = await _jobService.GetByIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<Job, JobResponse>(entity);
            return Ok(response);
        }

        // GET: api/Jobs/purchaseOrder/5
        [HttpGet("purchaseOrder/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<JobResponse>>> GetByPurchaseOrderId(int id)
        {
            var entity = await _jobService.GetByPurchaseOrderIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<List<Job>, List<JobResponse>>(entity);
            return Ok(response);
        }

        // POST: api/Jobs
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] JobCreateRequest request)
        {
            var entity = _mapper.Map<JobCreateRequest, Job>(request);

            await _jobService.InsertAsync(entity);
            var response = _mapper.Map<Job, JobResponse>(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, response);
        }
        
        
        // GET: api/jobs/types
        [HttpGet("types")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetJobTypes()
        {
            var tuples = Enum.GetValues(typeof(JobType)).Cast<object?>()
                .Select(enumVal => new Tuple<int, string>((int) enumVal, ((JobType)enumVal).GetAttributeOfType<DescriptionAttribute>()?.Description ?? enumVal.ToString()));
            return Ok(tuples.ToList());
        }
        
        
        // GET: api/jobs/statuses
        [HttpGet("statuses")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetStatuses()
        {
            var tuples = Enum.GetValues(typeof(JobStatus)).Cast<object?>()
                .Select(enumVal => new Tuple<int, string>((int) enumVal, ((JobStatus)enumVal).GetAttributeOfType<DescriptionAttribute>()?.Description ?? enumVal.ToString()));
            return Ok(tuples.ToList());
        }


        // PUT: api/Jobs/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] JobCreateRequest model)
        {
            var entity = await _jobService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _jobService.Update(entity);
            var response = _mapper.Map<Job, JobResponse>(entity);
            return Ok(response);
        }

        // DELETE: api/Jobs/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _jobService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _jobService.DeleteById(id);
            return NoContent();
        }
    }

    public class EnumResponseModel
    {
        public List<Tuple<int, string>> Values { get; set; }
    }
}