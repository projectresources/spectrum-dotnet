﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RegionsController : ControllerBase
    {
        private IRegionService _regionService;
        private IMapper _mapper;

        public RegionsController(IRegionService regionService, IMapper mapper)
        {
            _regionService = regionService;
            _mapper = mapper;
        }
        
        // GET: api/Regions
        [HttpGet]
        public async Task<IEnumerable<RegionResponse>> Get()
        {
            var regions = await _regionService.GetAllListAsync();
            var response = _mapper.Map<List<Region>, List<RegionResponse>>(regions);
            return response;
        }

        // GET: api/Regions/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<RegionResponse>> GetById(int id)
        {
            var region = await _regionService.GetByIdAsync(id);

            if (region == null)
            {
                return NotFound();
            }
            var response = _mapper.Map<Region, RegionResponse>(region);

            return Ok(response);
        }

        // POST: api/Regions
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] RegionCreateRequest request)
        {
            var entity = _mapper.Map<RegionCreateRequest, Region>(request);

            await _regionService.InsertAsync(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, entity);
        }

        // PUT: api/Regions/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] RegionCreateRequest model)
        {
            var entity = await _regionService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _regionService.Update(entity);
            return Ok(entity);
        }

        // DELETE: api/Regions/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _regionService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _regionService.DeleteById(id);
            return NoContent();
        }
        
        
        // GET: api/Regions/5
        [HttpGet("{id}/areas")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreaResponse>> GetAreasByCompanyId(int id)
        {
            var region = await _regionService.GetByIdAsync(id);

            if (region == null)
            {
                return NotFound();
            }

            var areas = region.Areas.ToList();
            var response = _mapper.Map<List<Area>, List<AreaResponse>>(areas);
            return Ok(response);
        }
    }
}