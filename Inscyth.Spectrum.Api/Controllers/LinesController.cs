﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class LinesController : ControllerBase
    {
        private ILineService _lineService;
        private IMapper _mapper;

        public LinesController(ILineService lineService, IMapper mapper)
        {
            _lineService = lineService;
            _mapper = mapper;
        }
        
        // GET: api/Lines
        [HttpGet]
        public async Task<IEnumerable<LineResponse>> Get()
        {
            var entities = await _lineService.GetAllListAsync();
            var responses = _mapper.Map<List<Line>, List<LineResponse>>(entities);
            return responses;
        }

        // GET: api/area/5
        [HttpGet("area/{areaId}")]
        public async Task<IEnumerable<LineResponse>> GetByAreaId(int areaId)
        {
            var entities = await _lineService.GetByAreaId(areaId);
            var responses = _mapper.Map<List<Line>, List<LineResponse>>(entities);
            return responses;
        }

        // GET: api/Lines/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<LineResponse>> GetById(int id)
        {
            var line = await _lineService.GetByIdAsync(id);

            if (line == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<Line, LineResponse>(line);
            return Ok(response);
        }

        // POST: api/Lines
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] LineCreateRequest request)
        {
            var entity = _mapper.Map<LineCreateRequest, Line>(request);

            await _lineService.InsertAsync(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, entity);
        }

        // PUT: api/Lines/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] LineCreateRequest model)
        {
            var entity = await _lineService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _lineService.Update(entity);

            var response = _mapper.Map<Line, LineResponse>(entity);
            return Ok(response);
        }

        // DELETE: api/Lines/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _lineService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _lineService.DeleteById(id);
            return NoContent();
        }
    }
}