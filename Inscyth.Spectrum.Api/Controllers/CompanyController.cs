﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CompanyController : ControllerBase
    {
        private ICompanyService _companyService;
        private IRegionService _regionService;
        private IMapper _mapper;
        private IHttpContextAccessor _httpContextAccessor;
        private UserManager<ApplicationUser> _userManager;
        private IUserService _userService;

        public CompanyController(ICompanyService companyService, 
            IRegionService regionService, 
            IMapper mapper, 
            IHttpContextAccessor httpContextAccessor,
            UserManager<ApplicationUser> userManager,
            IUserService userService)
        {
            _companyService = companyService;
            _regionService = regionService;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _userService = userService;
        }

        // GET: api/Company
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<CompanyResponse>> Get()
        {
            var entities = await _companyService.GetAllListAsync();
            return  _mapper.Map<List<Company>, List<CompanyResponse>>(entities);
        }

        // GET: api/Company/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CompanyResponse>> GetById(int id)
        {
            var company = await _companyService.GetByIdAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<Company, CompanyResponse>(company);

            return Ok(response);
        }
        
        // GET: api/Company/5/regions
        [HttpGet("{id}/regions")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<RegionResponse>> GetRegionsByCompanyId(int id)
        {
            var company = await _companyService.GetByIdAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            var regions = await _regionService.GetRegionsByCompanyId(id);
            var response = _mapper.Map<List<Region>, List<RegionResponse>>(regions);
            return Ok(response);
        }
        
        // GET: api/Company/5/purchaseorders
        [HttpGet("{id}/purchaseorders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PurchaseOrderResponse>> GetPurchaseOrdersByCompanyId(int id)
        {
            var company = await _companyService.GetByIdAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            var purchaseOrders = company.PurchaseOrders.ToList();
            var response = _mapper.Map<List<PurchaseOrder>, List<PurchaseOrderResponse>>(purchaseOrders);
            return Ok(response);
        }

        // POST: api/Company
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] CompanyCreateRequest request)
        {
            var entity = _mapper.Map<CompanyCreateRequest, Company>(request);

            var aspNetUserId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var identityUser = await _userManager.FindByIdAsync(aspNetUserId);
            var user = await _userService.GetByUserName(identityUser.UserName);

            entity.CreatedBy = user;

            await _companyService.InsertAsync(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, entity);
        }

        // PUT: api/Company/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] CompanyUpdateRequest model)
        {
            var entity = await _companyService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _companyService.Update(entity);
            var response = _mapper.Map<Company, CompanyResponse>(entity);
            return Ok(response);
        }

        // DELETE: api/Company/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _companyService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _companyService.DeleteById(id);
            return NoContent();
        }
    }
}