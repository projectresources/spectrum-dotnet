﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AreasController : ControllerBase
    {
        private IAreaService _areaService;
        private IMapper _mapper;

        public AreasController(IAreaService areaService, IMapper mapper)
        {
            _areaService = areaService;
            _mapper = mapper;
        }
        
        // GET: api/Areas
        [HttpGet]
        public async Task<IEnumerable<AreaResponse>> Get()
        {
            var entities = await _areaService.GetAllListAsync();
            var response = _mapper.Map<List<Area>, List<AreaResponse>>(entities);
            return response;
        }

        // GET: api/Areas/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreaResponse>> GetById(int id)
        {
            var area = await _areaService.GetByIdAsync(id);

            if (area == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<Area, AreaResponse>(area);
            return Ok(result);
        }

        // POST: api/Areas
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] AreaCreateRequest request)
        {
            var entity = _mapper.Map<AreaCreateRequest, Area>(request);

            await _areaService.InsertAsync(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, entity);
        }

        // PUT: api/Areas/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] AreaCreateRequest model)
        {
            var entity = await _areaService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _areaService.Update(entity);
            var response = _mapper.Map<Area, AreaResponse>(entity);
            return Ok(response);
        }

        // DELETE: api/Areas/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _areaService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _areaService.DeleteById(id);
            return NoContent();
        }
    }
}