﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.OpenApi.Extensions;
using System.ComponentModel;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private SpectrumDbContext _dbContext;
        private IUserService _userService;

        public UserController(UserManager<ApplicationUser> userManager, 
            IMapper mapper,  
            SpectrumDbContext dbContext,
            IUserService userService)
        {
            _userManager = userManager;
            _mapper = mapper;
            _dbContext = dbContext;
            _userService = userService;
        }

        // GET: api/User
        [HttpGet]
        public async Task<IEnumerable<UserResponse>> Get()
        {
            return (await _userService.GetAllListAsync()).Select(u => new UserResponse
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Company = u.Company?.Name,
                Email = u.Email,
                CompanyId = u.CompanyId,
                UserName = u.UserName,
                PhoneNumber = u.PhoneNumber
            });
        }
        
        // GET: api/User/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserResponse>> GetById(int id)
        {
            var user = await _userService.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            var response = new UserResponse
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Company = user.Company?.Name,
                Email = user.Email,
                CompanyId = user.CompanyId,
                UserName = user.UserName,
                UserType = user.UserType,
                Role = user.Role
            };
            
            return Ok(response);
        }
        
        // POST: api/User
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] UserRegistrationRequestModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var user = new ApplicationUser
            {
                Email = model.Email,
                UserName = model.UserName,
            };
            
            var result = await _userManager.CreateAsync(user, model.Password);

            var entity = _mapper.Map<UserRegistrationRequestModel, User>(model);
            if (!result.Succeeded) return BadRequest(result.Errors);
            
            await _userService.InsertAsync(entity);
            return Created(nameof(Post), model);
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateUserRequest model)
        {
            var entity = await _userService.GetByIdAsync(id);
            var identityUser = await _userManager.FindByEmailAsync(entity.Email);
            
            if (entity == null)
            {
                return NotFound();
            }

            _mapper.Map(model, entity);

            identityUser.Email = entity.Email;
            identityUser.UserName = entity.UserName;

            var result =  await _userManager.UpdateAsync(identityUser);

            if (!result.Succeeded) return BadRequest(result.Errors);

            if (!string.IsNullOrEmpty(model.Password))
            {
                var pwResult =
                    await _userManager.ChangePasswordAsync(identityUser, model.CurrentPassword, model.Password);

                if (!pwResult.Succeeded) return BadRequest(pwResult.Errors);
            }

            await _userService.Update(entity);
            return Ok(entity);

        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var result = await _userManager.DeleteAsync(user);

            if (result.Succeeded)
            {
                return NoContent();
                
            }

            return BadRequest(result.Errors);
        }

        // GET: api/User/types
        [HttpGet( "types" )]
        [ProducesResponseType( StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetUserTypes()
        {
            var tuples = Enum.GetValues( typeof( UserType ) ).Cast<object?>()
                .Select( enumVal => new Tuple<int, string>( (int)enumVal, ( (UserType)enumVal ).GetAttributeOfType<DescriptionAttribute>()?.Description ?? enumVal.ToString() ) );
            return Ok( tuples.ToList() );
        }


        // GET: api/User/roles
        [HttpGet( "roles/{userType}" )]
        [ProducesResponseType( StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetRoles( UserType userType )
        {
            var tuples = Enum.GetValues( typeof( Role ) ).Cast<object?>()
                .Select( enumVal => new Tuple<int, string>( (int)enumVal, ( (Role)enumVal ).GetAttributeOfType<DescriptionAttribute>()?.Description ?? enumVal.ToString() ) );
            var result = userType == UserType.SpectrumUser
                ? tuples.Where( t => (Role)t.Item1 == Role.Manager || (Role)t.Item1 == Role.Operator ).ToList()
                : tuples.Where( t => (Role)t.Item1 == Role.Supervisor || (Role)t.Item1 == Role.Technician || (Role)t.Item1 == Role.Inspector ).ToList();

            return Ok( result );
        }
    }
}