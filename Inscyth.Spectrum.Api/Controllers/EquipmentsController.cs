﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Extensions;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EquipmentsController : ControllerBase
    {
        private IEquipmentService _equipmentService;
        private IMapper _mapper;

        public EquipmentsController(IEquipmentService equipmentService, IMapper mapper)
        {
            _equipmentService = equipmentService;
            _mapper = mapper;
        }
        
        // GET: api/Equipments
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var entities = await _equipmentService.GetAllListAsync();
            var response = _mapper.Map<List<Equipment>, List<EquipmentResponse>>(entities);
            return Ok(response);
        }

        // GET: api/Equipments/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EquipmentResponse>> GetById(int id)
        {
            var equipment = await _equipmentService.GetByIdAsync(id);

            if (equipment == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<Equipment, EquipmentResponse>(equipment);
            return Ok(response);
        }

        // POST: api/Equipments
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] EquipmentCreateRequest request)
        {
            var entity = _mapper.Map<EquipmentCreateRequest, Equipment>(request);

            await _equipmentService.InsertAsync(entity);
            var response = _mapper.Map<Equipment, EquipmentResponse>(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, response);
        }

        
        // GET: api/equipments/statuses
        [HttpGet("types")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetEquipmentTypes(int id)
        {
            var tuples = Enum.GetValues(typeof(EquipmentType)).Cast<object?>()
                .Select(enumVal => new Tuple<int, string>((int) enumVal, ((EquipmentType)enumVal).GetAttributeOfType<DescriptionAttribute>()?.Description ?? enumVal.ToString()));
            return Ok(tuples.ToList());
        }


        // PUT: api/Equipments/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] EquipmentCreateRequest model)
        {
            var entity = await _equipmentService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _equipmentService.Update(entity);
            return Ok(entity);
        }

        // DELETE: api/Equipments/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _equipmentService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _equipmentService.DeleteById(id);
            return NoContent();
        }
    }
}