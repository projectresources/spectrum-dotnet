﻿using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserAuditsController : ControllerBase
    {
        private IUserAuditService _userAuditService;
        private IMapper _mapper;

        public UserAuditsController(IUserAuditService userAuditService, IMapper mapper)
        {
            _userAuditService = userAuditService;
            _mapper = mapper;
        }

        // GET: api/useraudits
        [HttpGet]
        public async Task<IEnumerable<UserAuditResponse>> Get()
        {
            var userAudits = await _userAuditService.GetAllListAsync();
            var response = _mapper.Map<List<UserAudit>, List<UserAuditResponse>>(userAudits.OrderByDescending(a => a.CreatedAt).ToList());
            return response;
        }
    }
}
