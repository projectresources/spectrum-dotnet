﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VehiclesController : ControllerBase
    {
        private IVehicleService _vehicleService;
        private IMapper _mapper;

        public VehiclesController(IVehicleService vehicleService, IMapper mapper)
        {
            _vehicleService = vehicleService;
            _mapper = mapper;
        }
        
        // GET: api/Vehicles
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var entities = await _vehicleService.GetAllListAsync();
            var response = _mapper.Map<List<Vehicle>, List<VehicleResponse>>(entities);
            return Ok(response);
        }

        // GET: api/Vehicles/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<VehicleResponse>> GetById(int id)
        {
            var vehicle = await _vehicleService.GetByIdAsync(id);

            if (vehicle == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<Vehicle, VehicleResponse>(vehicle);
            return Ok(response);
        }

        // POST: api/Vehicles
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] VehicleCreateRequest request)
        {
            var entity = _mapper.Map<VehicleCreateRequest, Vehicle>(request);

            await _vehicleService.InsertAsync(entity);
            var response = _mapper.Map<Vehicle, VehicleResponse>(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, response);
        }

        // PUT: api/Vehicles/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] VehicleCreateRequest model)
        {
            var entity = await _vehicleService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _vehicleService.Update(entity);
            var response = _mapper.Map<Vehicle, VehicleResponse>(entity);
            return Ok(response);
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _vehicleService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _vehicleService.DeleteById(id);
            return NoContent();
        }
    }
}