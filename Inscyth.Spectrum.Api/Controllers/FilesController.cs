﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Azure;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly ISpectrumFileService _fileService;
        private readonly IAzureStorageService _azureStorageService;
        private readonly IMapper _mapper;
        private readonly ILineService _lineService;

        public FilesController(ISpectrumFileService fileService,
            IAzureStorageService azureStorageService,
            ILineService lineService,
            IMapper mapper)
        {
            _lineService = lineService;
            _fileService = fileService;
            _azureStorageService = azureStorageService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<List<FileViewResponse>> Get()
        {
            var fileList = await _fileService.GetAllListAsync();
            var fileViewList = _mapper.Map<List<SpectrumFile>, List<FileViewResponse>>(fileList);

            return fileViewList;
        }

        [HttpPost("line/{lineId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FileViewResponse>> Post([FromForm] IFormFile file, int lineId)
        {
            var line = await _lineService.GetByIdAsync(lineId);

            if (line == null)
            {
                return NotFound(new {message = "Line not found"});
            }

            var cloudBlockBlob = await _azureStorageService.UploadFile(file, $"Line{line.Id}");

            var existing = await _fileService.GetByNameAsync(cloudBlockBlob.Name);
            var fileEntity = existing ?? new SpectrumFile();

            fileEntity.LineId = lineId;
            fileEntity.Url = cloudBlockBlob.Uri.AbsoluteUri;
            fileEntity.Name = cloudBlockBlob.Name;

            if (existing != null)
            {
                await _fileService.Update(existing);
            }
            else
            {
                await _fileService.InsertAsync(fileEntity);
            }

            return CreatedAtAction(nameof(GetById), new {id = fileEntity.Id},
                _mapper.Map<SpectrumFile, FileViewResponse>(fileEntity));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FileViewResponse>> GetById(int id)
        {
            var fileItem = await _fileService.GetByIdAsync(id);

            if (fileItem == null)
                return NotFound();

            var fileViewItem = _mapper.Map<SpectrumFile, FileViewResponse>(fileItem);
            return Ok(fileViewItem);
        }


        // GET: api/files/line/5
        [HttpGet("line/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<FileViewResponse>>> GetByLineId(int id)
        {
            var entity = await _fileService.GetByLineIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<List<SpectrumFile>, List<FileViewResponse>>(entity);
            return Ok(response);
        }
    }
}