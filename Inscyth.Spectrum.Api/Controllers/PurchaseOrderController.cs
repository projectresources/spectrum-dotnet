﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inscyth.Spectrum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PurchaseOrdersController : ControllerBase
    {
        private IPurchaseOrderService _purchaseOrderService;
        private IMapper _mapper;

        public PurchaseOrdersController(IPurchaseOrderService purchaseOrderService, IMapper mapper)
        {
            _purchaseOrderService = purchaseOrderService;
            _mapper = mapper;
        }
        
        // GET: api/PurchaseOrders
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var entities = await _purchaseOrderService.GetAllListAsync();
            var response = _mapper.Map<List<PurchaseOrder>, List<PurchaseOrderResponse>>(entities);
            return Ok(response);
        }

        // GET: api/PurchaseOrders/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PurchaseOrderResponse>> GetById(int id)
        {
            var entity = await _purchaseOrderService.GetByIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<PurchaseOrder, PurchaseOrderResponse>(entity);
            return Ok(response);
        }

        // POST: api/PurchaseOrders
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] PurchaseOrderCreateRequest request)
        {
            var entity = _mapper.Map<PurchaseOrderCreateRequest, PurchaseOrder>(request);

            await _purchaseOrderService.InsertAsync(entity);
            return CreatedAtAction(nameof(GetById), new { id = entity.Id }, entity);
        }

        // PUT: api/PurchaseOrders/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] PurchaseOrderCreateRequest model)
        {
            var entity = await _purchaseOrderService.GetByIdAsync(id);

            if (entity == null)
            {
                return BadRequest();
            }

            _mapper.Map(model, entity);
            
            await _purchaseOrderService.Update(entity);
            var response = _mapper.Map<PurchaseOrder, PurchaseOrderResponse>(entity);
            return Ok(entity);
        }

        // DELETE: api/PurchaseOrders/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _purchaseOrderService.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            await _purchaseOrderService.DeleteById(id);
            return NoContent();
        }
    }
}