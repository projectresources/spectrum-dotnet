﻿using System.ComponentModel;
using System.Linq;
using AutoMapper;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Models;
using Microsoft.OpenApi.Extensions;

namespace Inscyth.Spectrum.AutoMapperProfiles
{
    public class SpectrumProfile : Profile
    {
        public SpectrumProfile()
        {
            CreateMap<CompanyCreateRequest, Company>();
            CreateMap<CompanyUpdateRequest, Company>()
                .ForAllMembers(o =>
                    o.Condition((src, dest, value) => value != null));

            CreateMap<UserRegistrationRequestModel, User>();
            CreateMap<UpdateUserRequest, User>();

            CreateMap<AreaCreateRequest, Area>();
            CreateMap<EquipmentCreateRequest, Equipment>();
            CreateMap<JobCreateRequest, Job>();
            CreateMap<LineCreateRequest, Line>();
            CreateMap<PurchaseOrderCreateRequest, PurchaseOrder>();
            CreateMap<RegionCreateRequest, Region>();
            CreateMap<VehicleCreateRequest, Vehicle>();
            CreateMap<FileRequest, SpectrumFile>();
            CreateMap<User, UserResponse>();


            CreateMap<Region, RegionResponse>()
                .ForMember(r => r.Company, e =>
                    e.MapFrom((src => src.Company == null ? "" : src.Company.Name)));

            CreateMap<Area, AreaResponse>()
                .ForMember(r => r.CompanyId,
                    e => e.MapFrom(src => src.Region.CompanyId));

            CreateMap<Line, LineResponse>();
            CreateMap<Company, CompanyResponse>();
            CreateMap<PurchaseOrder, PurchaseOrderResponse>()
                .ForMember(r => r.Company, c => c.MapFrom(src => src.Company.Name))
                .ForMember(r => r.JobIds,
                    e => e.MapFrom(src => src.Jobs.Select(a => a.Id)));

            CreateMap<Job, JobResponse>()
                .ForMember(j => j.JobTypeText,
                    src => src.MapFrom(j =>
                        j.JobType.GetAttributeOfType<DescriptionAttribute>() != null
                            ? j.JobType.GetAttributeOfType<DescriptionAttribute>().Description
                            : j.JobType.ToString()))
                .ForMember(j => j.JobStatusText,
                    src => src.MapFrom(j =>
                        j.JobStatus.GetAttributeOfType<DescriptionAttribute>() != null
                            ? j.JobStatus.GetAttributeOfType<DescriptionAttribute>().Description
                            : j.JobStatus.ToString()));

            CreateMap<Equipment, EquipmentResponse>()
                .ForMember(j => j.TypeText,
                    src => src.MapFrom(j =>
                        j.Type.GetAttributeOfType<DescriptionAttribute>() != null
                            ? j.Type.GetAttributeOfType<DescriptionAttribute>().Description
                            : j.Type.ToString()));

            CreateMap<Vehicle, VehicleResponse>();
            CreateMap<SpectrumFile, FileViewResponse>();
            CreateMap<UserAudit, UserAuditResponse>();
        }
    }
}