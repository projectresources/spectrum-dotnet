﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Inscyth.Spectrum.AutoMapperProfiles;
using Inscyth.Spectrum.Controllers;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Models;
using Inscyth.Spectrum.Services;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Tests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Inscyth.Spectrum.Api.Tests
{
    public class CompanyControllerTests
    {
        
        //[Fact]
        public async Task GetAll_HappyPath_ShouldReturnSuccess()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name"),
                new Company(name: "Fake Name2"),
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetAllListAsync()).Returns(Task.FromResult(companies));
            
            var configuration = new MapperConfiguration(cfg =>
                cfg.AddProfile(new SpectrumProfile()));
            var mapper  = new Mapper(configuration);
            
            
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mapper, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var actionResult = await controller.Get();
            actionResult.Should().NotBeEmpty();
        }
        
        
        //[Fact]
        public async Task GetById_HappyPath_ShouldReturn200()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult( companies.FirstOrDefault(h => h.Id == id)));
            
            var configuration = new MapperConfiguration(cfg =>
                cfg.AddProfile(new SpectrumProfile()));
            var mapper  = new Mapper(configuration);
            
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mapper, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var result = await controller.GetById(1);
            var actionResult = result.Result;

            actionResult.Should().BeOfType<OkObjectResult>().Subject.StatusCode.Should().Be(200);
            actionResult.Should().BeOfType<OkObjectResult>().Subject.Value.Should().BeEquivalentTo(mapper.Map<Company, CompanyResponse>(companies.FirstOrDefault(h => h.Id == 1)));
        }
        
        //[Fact]
        public async Task GetById_CompanyNonExistent_ShouldReturn404()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(companies.FirstOrDefault(h => h.Id == id)));
            
            var mockedMapper = new Mock<IMapper>();
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var result = await controller.GetById(3);
            result.Result.Should().BeOfType<NotFoundResult>();
        }

        //[Fact]
        public async Task Create_ValidCompany_ShouldReturnSuccess()
        {
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.InsertAsync(It.IsAny<Company>()));
            
            var createRequest = new CompanyCreateRequest("test hp1");
            
            var mockedMapper = new Mock<IMapper>();
            mockedMapper.Setup(m => m.Map<CompanyCreateRequest, Company>(It.IsAny<CompanyCreateRequest>()))
                .Returns(new Company(createRequest.Name));
            
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var result = await controller.Post(createRequest);
            result.Should().BeOfType<CreatedAtActionResult>().Subject.Value.Should().NotBeNull()
                .And.Subject.Should().BeOfType<Company>().Should().NotBeNull();
        }
        
        //[Fact]
        public async Task Put_CompanyExisting_ShouldReturn200()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(companies.FirstOrDefault(h => h.Id == id)));
            mockedService.Setup(s => s.Update(It.IsAny<Company>()));

            var mockedMapper = new Mock<IMapper>();
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var companyUpdateRequest = new CompanyUpdateRequest(){Name = "test"};
            
            mockedMapper.Setup(m => m.Map<CompanyCreateRequest, Company>(It.IsAny<CompanyCreateRequest>()))
                .Returns(new Company(companyUpdateRequest.Name));
            
            var result = await controller.Put(1, companyUpdateRequest);
            result.Should().BeOfType<OkObjectResult>();
        }

        //[Fact]
        public async Task Put_CompanyNonExistent_ShouldReturnBadRequestResult()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(companies.FirstOrDefault(h => h.Id == id)));
            
            mockedService.Setup(s => s.Update(It.IsAny<Company>()));

            var mockedMapper = new Mock<IMapper>();
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var companyUpdateRequest = new CompanyUpdateRequest();
            
            var result = await controller.Put(3, companyUpdateRequest);
            result.Should().BeOfType<BadRequestResult>();
        }
        
        //[Fact]
        public async Task Delete_CompanyExisting_ShouldReturnNoContent()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(companies.FirstOrDefault(h => h.Id == id)));
            mockedService.Setup(s => s.DeleteById(It.IsAny<int>()));

            var mockedMapper = new Mock<IMapper>();
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var result = await controller.Delete(1);
            result.Should().BeOfType<NoContentResult>();
        }
        
        //[Fact]
        public async Task Delete_CompanyExisting_ShouldReturnNotFound()
        {
            var companies = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                    Id = 1
                },
                new Company(name: "Fake Name2")
                {
                    Id = 2
                },
            };
            
            var mockedService = new Mock<ICompanyService>();
            mockedService.Setup(s => s.GetByIdAsync(It.IsAny<int>()))
                .Returns<int>(id => Task.FromResult(companies.FirstOrDefault(h => h.Id == id)));
            mockedService.Setup(s => s.DeleteById(It.IsAny<int>()));

            var mockedMapper = new Mock<IMapper>();
            var controller = new CompanyController(mockedService.Object, new Mock<IRegionService>().Object, mockedMapper.Object, Mock.Of<IHttpContextAccessor>(), Mock.Of<UserManager<ApplicationUser>>(), Mock.Of<IUserService>());
            var result = await controller.Delete(3);
            result.Should().BeOfType<NotFoundResult>();
        }
    }
}