﻿using System.Threading.Tasks;
using IdentityServer4.Services;
using IdentityServer4.Events;
using Inscyth.Spectrum.IdentityServer.Data;
using Inscyth.Spectrum.IdentityServer.Models;
using Microsoft.AspNetCore.Http;

namespace Inscyth.Spectrum.IdentityServer.Services
{
    public class AuditEventService : IEventService

    {
        private readonly IdentityDbContext _db;
        private HttpContext _httpContext;

        public AuditEventService(IdentityDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            _db = dbContext;
            _httpContext = httpContextAccessor.HttpContext;
        }

        public async Task RaiseAsync(Event evt)
        {
            var newAuditRecord = evt switch
            {
                UserLoginSuccessEvent cast => UserAudit.CreateAuditEvent(cast.Username, cast.EventType,
                    _httpContext.Connection.RemoteIpAddress.ToString(), cast.Name),
                UserLoginFailureEvent cast => UserAudit.CreateAuditEvent(cast.Username, cast.EventType,
                    _httpContext.Connection.RemoteIpAddress.ToString(), cast.Name),
                _ => null
            };

            if (newAuditRecord == null)
            {
                // Dont save
                return;
            }

            _db.UserAuditEvents.Add(newAuditRecord);
            await _db.SaveChangesAsync();
        }

        public bool CanRaiseEventType(EventTypes evtType)
        {
            return evtType == EventTypes.Success || evtType == EventTypes.Failure;
        }
    }
}