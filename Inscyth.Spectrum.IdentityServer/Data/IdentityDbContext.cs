﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Inscyth.Spectrum.IdentityServer.Models;
using Microsoft.Azure.Services.AppAuthentication;

namespace Inscyth.Spectrum.IdentityServer.Data
{
    public class IdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public IdentityDbContext(DbContextOptions<IdentityDbContext> options)
            : base(options)
        {
            try
            {
                Console.WriteLine($"Trying Azure SQL token Generator...");
                Console.WriteLine($"Using ConnectionString: {Database.GetDbConnection().ConnectionString}");
                var conn = (Microsoft.Data.SqlClient.SqlConnection)Database.GetDbConnection();
                conn.AccessToken = (new AzureServiceTokenProvider()).GetAccessTokenAsync("https://database.windows.net/").Result;
            }
            catch (Exception)
            {
                // Skip if it is not an azure SQL database
            }
            
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<UserAudit> UserAuditEvents { get; set; }
    }
}
