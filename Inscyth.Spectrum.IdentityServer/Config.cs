﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System.Collections;
using IdentityServer4.Models;
using System.Collections.Generic;
using IdentityServer4;
using Microsoft.Extensions.Configuration;

namespace Inscyth.Spectrum.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(), 
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource("spectrum.api"),
            };

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            return new Client[]
            {
                // JavaScript Client
                new Client
                {
                    ClientId = "spa",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    RequireConsent = false,

                    RedirectUris = configuration.GetSection("IdentityServer:RedirectUris").Get<string[]>(),
                    PostLogoutRedirectUris =  configuration.GetSection("IdentityServer:PostLogoutRedirectUris").Get<string[]>(),
                    AllowedCorsOrigins = configuration.GetSection("IdentityServer:AllowedCorsOrigins").Get<string[]>(),

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "spectrum.api"
                    }
                }
            };
        }
        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // JavaScript Client
                new Client
                {
                    ClientId = "spa",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris = {"http://localhost:4200/callback", "http://localhost:4200/"},
                    PostLogoutRedirectUris = {"http://localhost:4200/"},
                    AllowedCorsOrigins = {"http://localhost:4200"},

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "spectrum.api"
                    }
                }
            };
    }
}