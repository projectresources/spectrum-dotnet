﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using SendGrid.Helpers.Mail;

namespace Inscyth.Spectrum.Services.Email
{
    public interface IEmailMessageFactory
    {
        SendGridMessage Create<T>(Dictionary<string, string> parameters) where T : IEmailTemplate, new();
    }

    public class EmailMessageFactory : IEmailMessageFactory
    {
        private readonly EmailOptions _emailConfig;

        public EmailMessageFactory(IOptions<EmailOptions> emailConfig)
        {
            _emailConfig = emailConfig.Value;
        }
        public SendGridMessage Create<T>(Dictionary<string, string> parameters) where T : IEmailTemplate, new()
        {
            var template = new T();
            var content = parameters.Keys.Aggregate(template.HtmlContent, (current, token) => current.Replace(token, parameters[token]));

            return new SendGridMessage
            {
                From = new EmailAddress(_emailConfig.FromEmail, _emailConfig.FromName),
                Subject = template.Subject,
                HtmlContent = content
            };
        }
    }

    public class EmailOptions
    {
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string ApiKey { get; set; }
    }
}
