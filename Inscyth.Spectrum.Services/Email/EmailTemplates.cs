﻿namespace Inscyth.Spectrum.Services.Email
{
    public interface IEmailTemplate
    {
        public string Subject { get; }
        public string HtmlContent { get; }
    }

    public class ChangePasswordTemplate : IEmailTemplate
    {
        public string Subject => "Change password request";

        public string HtmlContent =>
            "We are sending this email because you are requesting for a password change. Please click this <a href='#URL#'> link</a>";
    }
}