﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface ISpectrumFileService : IGenericRepository<SpectrumFile>
    {
        Task<List<SpectrumFile>> GetByLineIdAsync(int id);
        Task<SpectrumFile> GetByNameAsync(string uriAbsoluteUri);
    }
}