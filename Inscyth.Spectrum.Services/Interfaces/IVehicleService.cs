﻿using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IVehicleService : IGenericRepository<Vehicle>
    {
    }
}