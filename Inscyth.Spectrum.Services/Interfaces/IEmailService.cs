﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using SendGrid.Helpers.Mail;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IEmailService
    {
        Task<HttpStatusCode> SendAsync(string email, string userName, SendGridMessage message);
    }
}