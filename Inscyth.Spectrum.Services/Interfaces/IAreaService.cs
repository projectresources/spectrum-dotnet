﻿using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;

namespace Inscyth.Spectrum.Services.Services
{
    public interface IAreaService : IGenericRepository<Area>
    {
    }
}