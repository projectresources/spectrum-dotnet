﻿using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IUserAuditService : IGenericRepository<UserAudit>
    {
    }
}
