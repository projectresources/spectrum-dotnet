﻿using System.Net;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IChangePasswordService : IGenericRepository<ConfirmationEmail>
    {
        Task<HttpStatusCode> SendChangePasswordEmail(string emailAddress);
        Task<bool> ValidateGuid(string emailAddress, string guid);
    }
}