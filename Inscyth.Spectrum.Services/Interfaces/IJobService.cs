﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IJobService : IGenericRepository<Job>
    {
        Task<List<Job>> GetByPurchaseOrderIdAsync(int id);
    }
}