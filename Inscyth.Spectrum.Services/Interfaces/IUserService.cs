﻿using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using System.Threading.Tasks;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IUserService : IGenericRepository<User>
    {
        Task<User> GetByUserName(string userName);
    }
}