﻿using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inscyth.Spectrum.Services.Interfaces
{
    public interface IRegionService : IGenericRepository<Region>
    {
        Task<List<Region>> GetRegionsByCompanyId(int id);
    }
}