﻿using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class EquipmentService : GenericRepository<Equipment, SpectrumDbContext>, IEquipmentService
    {
        public EquipmentService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }
    }
}