﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class CompanyService : GenericRepository<Company, SpectrumDbContext>, ICompanyService
    {
        public CompanyService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }
        
        public async Task<Company> GetByIdAsync(int id)
        {
            return await _dbContext.Set<Company>()
                .Include(a => a.Regions)
                .Include(a => a.PurchaseOrders)
                .Include(a => a.CreatedBy)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public new async Task<List<Company>> GetAllListAsync()
        {
            return await _dbContext.Set<Company>()
                .Include(a => a.Regions)
                .Include(a => a.PurchaseOrders)
                .Include(a => a.CreatedBy).ToListAsync();
        }
    }
}