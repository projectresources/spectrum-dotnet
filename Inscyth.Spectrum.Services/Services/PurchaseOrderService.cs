﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class PurchaseOrderService : GenericRepository<PurchaseOrder, SpectrumDbContext>, IPurchaseOrderService
    {
        public PurchaseOrderService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public new async Task<List<PurchaseOrder>> GetAllListAsync()
        {
            return await _dbContext.PurchaseOrders
                .Include(p => p.Company)
                .Include(p => p.Jobs)
                .ToListAsync();
        }
        public new async Task<PurchaseOrder> GetByIdAsync(int id)
        {
            return await _dbContext.PurchaseOrders
                .Include(p => p.Company)
                .Include(p => p.Jobs)
                .FirstAsync(p => p.Id == id);
        }
    }
}