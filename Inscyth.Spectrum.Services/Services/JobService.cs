﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class JobService : GenericRepository<Job, SpectrumDbContext>, IJobService
    {
        public JobService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }
        
        public new async Task<List<Job>> GetAllListAsync()
        {
            return await _dbContext.Jobs
                .Include(r => r.PurchaseOrder)
                    .ThenInclude(po => po.Company)
                .Include(r => r.JobLines)
                .Include(r => r.JobUsers)
                .ToListAsync();
        }

        public new async Task<Job> GetByIdAsync(int id)
        {
            return await _dbContext.Jobs
                .Include(r => r.PurchaseOrder)
                    .ThenInclude(po => po.Company)
                .Include(r => r.JobLines)
                .Include(r => r.JobUsers)
                .FirstAsync(r => r.Id == id);
        }

        public async Task<List<Job>> GetByPurchaseOrderIdAsync(int id)
        {
            return await _dbContext.Jobs
                .Include(r => r.PurchaseOrder)
                .ThenInclude(po => po.Company)
                .Include(r => r.JobLines)
                .Include(r => r.JobUsers)
                .Where(r => r.PurchaseOrderId == id)
                .ToListAsync();
        }
    }
}