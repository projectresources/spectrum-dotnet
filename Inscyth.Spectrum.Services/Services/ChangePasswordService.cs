﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Email;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Inscyth.Spectrum.Services.Services
{
    public class ChangePasswordService : GenericRepository<ConfirmationEmail, SpectrumDbContext>, IChangePasswordService
    {
        private readonly IEmailService _emailService;
        private readonly IEmailMessageFactory _emailMessageFactory;

        public ChangePasswordService(IEmailService emailService, IEmailMessageFactory emailMessageFactory, SpectrumDbContext dbContext) : base(dbContext)
        {
            _emailService = emailService;
            _emailMessageFactory = emailMessageFactory;
        }

        public async Task<HttpStatusCode> SendChangePasswordEmail(string emailAddress)
        {
            try
            {
                var guid = Guid.NewGuid().ToString();
                var emailParam = new Dictionary<string, string>()
                {
                    {"#URL#", guid}
                };

                var user = await _dbContext.ApplicationUsers.FirstOrDefaultAsync(u => u.NormalizedEmail == emailAddress);

                if (user == null)
                {
                    return HttpStatusCode.BadRequest;
                }
                
                var emailMessage = _emailMessageFactory.Create<ChangePasswordTemplate>(emailParam);
                return await _emailService.SendAsync(emailAddress, user.UserName, emailMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> ValidateGuid(string emailAddress, string guid)
        {
            var email = await _dbContext.ConfirmationEmails.Where(e => e.Type == ConfirmationEmailType.ForgotPassword)
                .FirstOrDefaultAsync(e => e.EmailAddress == emailAddress && e.Guid == guid);

            if (email == null) return false;
            email.IsActive = false;
            await _dbContext.SaveChangesAsync();
            return true;

        }
    }
}