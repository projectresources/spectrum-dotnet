﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class RegionService : GenericRepository<Region, SpectrumDbContext>, IRegionService
    {
        public RegionService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public new async Task<List<Region>> GetAllListAsync()
        {
            return await _dbContext.Regions
                .Include(r => r.Company)
                .Include(r => r.Areas)
                .ThenInclude(a => a.Lines)
                .ThenInclude(l => l.Files)
                .ToListAsync();
        }

        public new async Task<Region> GetByIdAsync(int id)
        {
            return await _dbContext.Regions
                .Include(r => r.Company)
                .Include(r => r.Areas)
                .ThenInclude(a => a.Lines)
                .ThenInclude(l => l.Files)
                .FirstAsync(r => r.Id == id);
        }

        public async Task<List<Region>> GetRegionsByCompanyId(int id)
        {
            return await _dbContext.Regions
                .Include(r => r.Company)
                .Include(r => r.Areas)
                .ThenInclude(a => a.Lines)
                .ThenInclude(l => l.Files)
                .Where(r => r.CompanyId == id)
                .ToListAsync();
        }
    }
}