﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class AreaService : GenericRepository<Area, SpectrumDbContext>, IAreaService
    {
        public AreaService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Area>> GetAllListAsync()
        {
            return await _dbContext.Set<Area>()
                .Include(a => a.Lines)
                .Include(a => a.Region)
                .ThenInclude(r => r.Company)
                .ToListAsync();
        }

        public async Task<Area> GetByIdAsync(int id)
        {
            return await _dbContext.Set<Area>()
                .Include(a => a.Lines)
                .Include(a => a.Region)
                .ThenInclude(r => r.Company)
                .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}