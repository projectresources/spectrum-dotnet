﻿using System.Net;
using System.Threading.Tasks;
using Inscyth.Spectrum.Services.Email;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Inscyth.Spectrum.Services.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailOptions _emailConfig;

        public EmailService(IOptions<EmailOptions> emailConfig)
        {
            _emailConfig = emailConfig.Value;
        }

        public async Task<HttpStatusCode> SendAsync(string email, string userName, SendGridMessage msg)
        {
            var client = new SendGridClient(_emailConfig.ApiKey);
            msg.AddTo(new EmailAddress(email, userName));
            var response = await client.SendEmailAsync(msg);
            return response.StatusCode;
        }
    }
}