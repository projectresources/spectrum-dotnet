﻿using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class UserAuditService : GenericRepository<UserAudit, SpectrumDbContext>, IUserAuditService
    {
        public UserAuditService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }
    }
}