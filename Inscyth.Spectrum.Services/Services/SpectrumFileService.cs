﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class SpectrumFileService : GenericRepository<SpectrumFile, SpectrumDbContext>, ISpectrumFileService
    {
        public SpectrumFileService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<SpectrumFile>> GetByLineIdAsync(int id)
        {
                return await _dbContext.Files
                    .Include(f => f.Line)
                    .ToListAsync();
        }

        public async Task<SpectrumFile> GetByNameAsync(string name)
        {
            return await _dbContext.Files
                .Include(f => f.Line)
                .FirstOrDefaultAsync(f => f.Name == name);
        }
    }
}