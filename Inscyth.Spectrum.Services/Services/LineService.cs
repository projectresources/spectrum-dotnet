﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class LineService : GenericRepository<Line, SpectrumDbContext>, ILineService
    {
        public LineService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public new async Task<List<Line>> GetAllListAsync()
        {
            return await _dbContext.Lines
                .Include(l => l.Files)
                .Include(l => l.Area)
                    .ThenInclude(a => a.Region)
                    .ThenInclude(r => r.Company)
                .ToListAsync();
        }

        public new async Task<List<Line>> GetByAreaId(int id)
        {
            return await _dbContext.Lines
                .Include(l => l.Files)
                .Include(l => l.Area)
                    .ThenInclude(a => a.Region)
                    .ThenInclude(r => r.Company)
                .Where(l => l.AreaId == id)
                .ToListAsync();
        }

        public new async Task<Line> GetByIdAsync(int id)
        {
            return await _dbContext.Lines
                .Include(l => l.Files)
                .Include(l => l.Area)
                    .ThenInclude(a => a.Region)
                    .ThenInclude(r => r.Company)
                .FirstOrDefaultAsync(l => l.Id == id);
        }
    }
}