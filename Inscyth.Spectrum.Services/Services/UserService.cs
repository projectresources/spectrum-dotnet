﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Inscyth.Spectrum.Services.Services
{
    public class UserService : GenericRepository<User, SpectrumDbContext>, IUserService
    {
        public UserService(SpectrumDbContext dbContext) : base(dbContext)
        {
        }

        public new List<User> GetAll()
        {
            return _dbContext.Set<User>().Include(c => c.Company).ToList();
        }

        public async Task<List<User>> GetAllListAsync()
        {
            return await _dbContext.Set<User>().Include(c => c.Company).ToListAsync();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _dbContext.Set<User>().Include(c => c.Company).FirstAsync(u => u.Id == id);
        }

        public async Task<User> GetByUserName(string userName)
        {
            return await _dbContext.Set<User>().SingleOrDefaultAsync(u => u.UserName == userName);
        }

    }
}