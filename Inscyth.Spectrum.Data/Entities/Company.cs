﻿using System.Collections;
using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Company : ModelBase
    {
        public Company(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CompanyWebsite { get; set; }
        public string ContactName { get; set; }

        public int? PrimaryContactId { get; set; }
        public virtual User? PrimaryContact { get; set; }
        public ICollection<Region> Regions { get; set; }
        public ICollection<Job> Jobs { get; set; }
        public ICollection<PurchaseOrder> PurchaseOrders { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<CompanyContact> CompanyContacts { get; set; }
        public ICollection<User> Users { get; set; }
        public int? CreatedById { get; internal set; }
    }

    public enum CompanyType
    {
        Internal = 1,
        External = 2,
    }
}