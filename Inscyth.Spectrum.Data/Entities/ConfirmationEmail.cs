﻿using System.ComponentModel.DataAnnotations;

namespace Inscyth.Spectrum.Data.Entities
{
    public class ConfirmationEmail : ModelBase
    {
        public ConfirmationEmail(string emailAddress, string guid, ConfirmationEmailType type, bool isActive = true)
        {
            Guid = guid;
            EmailAddress = emailAddress;
            Type = type;
            IsActive = isActive;
        }
        
        [MaxLength(50)]
        public string Guid { get; set; }
        [MaxLength(200)]
        public string EmailAddress  { get; set; }
        public ConfirmationEmailType Type { get; set; }
        public bool IsActive { get; set; }
    }
    
    public enum ConfirmationEmailType
    {
        Confirmation,
        ForgotPassword
    }
}