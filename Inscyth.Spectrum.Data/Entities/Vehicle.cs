﻿using System.Collections;
using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Vehicle : ModelBase
    {
        public string? Vin { get; set; }
        public string? Notes { get; set; }
        public int DoeId { get; set; }
        public int VehicleYear { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        
        public ICollection<MileageLog> MileageLogs { get; set; }
        public ICollection<MaintenanceLog> MaintenanceLogs { get; set; }
    }
}