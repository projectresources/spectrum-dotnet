﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Equipment : ModelBase
    {
        public EquipmentType Type { get; set; }
        public string Name { get; set; }
        public string? SerialNumber { get; set; }
        public string? Description { get; set; }
        public string? Instructions { get; set; }
        public string? SoftwareReferenceLink { get; set; }
        public DateTime? LastServiceDate { get; set; }
        public bool IsOutOfService { get; set; }
        
        public ICollection<EquipmentLog> EquipmentLogs { get; set; }
    }

    public enum EquipmentType
    {
        Locator = 1,
        Transmitter,
        FieldPC,
        PC,
        Vehicle,
        Interruptor
    }
}