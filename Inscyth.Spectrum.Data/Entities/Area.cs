﻿using System.Collections;
using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Area : ModelBase
    {
        public string Name { get; set; }
        
        public string? LocStartDescription { get; set; }
        public string? LocEndDescription { get; set; }
        
        public double? LocationStart { get; set; }
        public double? LocationEnd { get; set; }
        
        public int RegionId { get; set; }
        public virtual Region Region { get; set; }
        public ICollection<Line> Lines { get; set; }
    }
}