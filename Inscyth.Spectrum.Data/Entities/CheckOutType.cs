﻿using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class CheckOutType : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public ICollection<EquipmentLog> EquipmentLogs { get; set; }
    }
}