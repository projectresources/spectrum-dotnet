﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class MaintenanceLog : ModelBase
    {
        public int ServiceTypeId { get; set; }
        public virtual ServiceType ServiceType { get; set; }
        
        public string Notes { get; set; }
        
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}