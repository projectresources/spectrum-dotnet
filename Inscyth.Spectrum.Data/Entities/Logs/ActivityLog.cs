﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class ActivityLog : ModelBase
    {
        public LogCategory Category { get; set; }
        public string Description { get; set; }
        
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }

    public enum LogCategory
    {
    }
}