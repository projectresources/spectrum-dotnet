﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class EquipmentLog : ModelBase
    {
        public CheckOutType CheckOutType { get; set; }
        public string Notes { get; set; }
        
        public int JobId { get; set; }
        public Job Job { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }
        
        public int EquipmentId { get; set; }
        public virtual Equipment Equipment { get; set; }
    }
}