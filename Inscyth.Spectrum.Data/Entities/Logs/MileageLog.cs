﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class MileageLog : ModelBase
    {
        public double StartOdometer { get; set; }
        public double EndOdometer { get; set; }
        public string Notes { get; set; }
        
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}