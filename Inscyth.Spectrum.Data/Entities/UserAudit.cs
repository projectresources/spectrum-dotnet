﻿using System;
using System.ComponentModel.DataAnnotations;
using IdentityServer4.Events;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Inscyth.Spectrum.Data.Entities
{
    public class UserAudit : ModelBase
    {
        [Required] public string UserId { get; private set; }

        [Required] public DateTimeOffset Timestamp { get; private set; } = DateTime.UtcNow;

        [Required] public EventTypes AuditEvent { get; set; }
        
        [Required] public string Name { get; set; }

        public string IpAddress { get; private set; }

        public static UserAudit CreateAuditEvent(string userId, EventTypes auditEventType, string ipAddress, string name)

        {
            return new UserAudit {UserId = userId, AuditEvent = auditEventType, IpAddress = ipAddress, Name = name};
        }
    }
}