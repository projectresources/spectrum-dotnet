﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class JobLine : ModelBase
    {
        public int JobId { get; set; }
        public virtual Job Job { get; set; }
        
        public int LineId { get; set; }
        public virtual Line Line { get; set; }
        
        public int AreaId { get; set; }
        public virtual Area Area { get; set; }
        
        // TODO: What is JobLinkId and JobLinkType? Is this link to the file system?
    }
}