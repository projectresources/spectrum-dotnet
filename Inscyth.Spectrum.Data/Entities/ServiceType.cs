﻿using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class ServiceType : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public ICollection<MaintenanceLog> MaintenanceLogs { get; set; }
    }
}