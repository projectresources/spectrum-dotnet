﻿using System.Collections;
using System.Collections.Generic;

namespace Inscyth.Spectrum.Data.Entities
{
    public class PurchaseOrder : ModelBase
    {
        public string Name { get; set; }
        
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public ICollection<Job> Jobs { get; set; }
    }
}