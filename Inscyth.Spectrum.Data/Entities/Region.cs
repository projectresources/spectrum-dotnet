﻿using System.Collections;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Region : ModelBase
    {
        public string Name { get; set; }

        public string LocStartDescription { get; set; }
        public string LocEndDescription { get; set; }
        
        public double? LocationStart { get; set; }
        public double? LocationEnd { get; set; }
        
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public ICollection<Area> Areas { get; set; }
    }
}