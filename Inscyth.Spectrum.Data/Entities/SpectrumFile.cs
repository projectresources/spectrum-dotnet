﻿using System.ComponentModel.DataAnnotations;

namespace Inscyth.Spectrum.Data.Entities
{
    public class SpectrumFile : ModelBase
    {
        [MaxLength(500)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Url { get; set; }
        
        public Line Line { get; set; }
        public int LineId { get; set; }
    }
}