﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class CompanyContact : ModelBase
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        
        public string Phone { get; set; }
        public string Email { get; set; }
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}