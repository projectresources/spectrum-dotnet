﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class JobUser : ModelBase
    {
        public virtual Job Job { get; set; }
        
        public int UserId { get; set; }
        public virtual User User { get; set; }
        
        // TODO: What is JobLinkId and JobLinkType? Is this link to the file system?
    }
}