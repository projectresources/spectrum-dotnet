﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Inscyth.Spectrum.Data.Entities
{
    public class User : ModelBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string? Birthday { get; set; }
        public UserType UserType { get; set; }
        public Role Role { get; set; }

        public int? CompanyId { get; set; }
        public Company? Company { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<JobUser> JobUsers { get; set; }
        public ICollection<ActivityLog> ActivityLogs { get; set; }
        public ICollection<EquipmentLog> EquipmentLogs { get; set; }
        public ICollection<Company> ContactToCompanies { get; set; }
        public ICollection<Company> CreatedCompanies { get; set; }
    }

    public enum UserType
    {
        [Description( "Spectrum User" )]
        SpectrumUser = 1,
        [Description( "Client User" )]
        ClientUser
    }

    public enum Role
    {
        Manager = 1,
        Operator,
        Supervisor,
        Technician,
        Inspector
    }
}