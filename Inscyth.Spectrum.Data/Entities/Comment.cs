﻿namespace Inscyth.Spectrum.Data.Entities
{
    public class Comment : ModelBase
    {
        public string Message { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsUrgent { get; set; }

        public int ParentId { get; set; }
        public virtual Comment Parent { get; set; }
        
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
        
        public int ChildId { get; set; }
        public virtual Comment Child { get; set; }
    }
}