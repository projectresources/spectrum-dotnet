﻿using System.Collections;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Line : ModelBase
    {
        public string Name { get; set; }
        
        public double? TotalMiles { get; set; }
        public double? TotalFeet { get; set; }
     
        public string? StartDescription { get; set; }
        public string? EndDescription { get; set; }   
        
        public double? Start { get; set; }
        public double? End { get; set; }
        
        public string LineType { get; set; }
        public virtual Area Area { get; set; }
        public int AreaId { get; set; }
        
        public ICollection<SpectrumFile> Files { get; set; }
    }
}