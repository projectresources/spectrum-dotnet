﻿using Microsoft.AspNetCore.Identity;

namespace Inscyth.Spectrum.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        
    }
}