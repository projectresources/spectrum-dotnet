﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Inscyth.Spectrum.Data.Entities
{
    public class Job : ModelBase
    {
        public string? Description { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public int PurchaseOrderId { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        
        public JobStatus JobStatus { get; set; }
        public JobType JobType { get; set; }
        public ICollection<JobLine> JobLines { get; set; }
        public ICollection<JobUser> JobUsers { get; set; }
    }

    public enum JobType
    {
        Interrupted = 1,
        Static,
        [Description("Prove Up")]
        ProveUp,
        [Description("Data Processing")]
        DataProcessing,
        Consulting,
        Technician,
        Inspector,
        Other
    }
    public enum JobStatus
    {
        Scheduled = 1,
        [Description("Survey Started")]
        SurveyStarted,
        [Description("Survey Complete")]
        SurveyComplete,
        
        [Description("Data Processing And QC")]
        DataProcessingAndQC,
        [Description("Data Finalized")]
        DataFinalized
    }
}