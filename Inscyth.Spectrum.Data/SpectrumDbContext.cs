﻿using System;
using System.Text.RegularExpressions;
using Inscyth.Spectrum.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.EntityFrameworkCore;

namespace Inscyth.Spectrum.Data
{
    public class SpectrumDbContext : IdentityDbContext<ApplicationUser>
    {
        public SpectrumDbContext(DbContextOptions<SpectrumDbContext> options)
            : base(options)
        {
            try
            {
                Console.WriteLine($"Trying Azure SQL token Generator...");
                Console.WriteLine($"Using ConnectionString: {Database.GetDbConnection().ConnectionString}");
                var conn = (Microsoft.Data.SqlClient.SqlConnection)Database.GetDbConnection();
                conn.AccessToken = (new AzureServiceTokenProvider()).GetAccessTokenAsync("https://database.windows.net/").Result;
            }
            catch (Exception)
            {
                // Skip if it is not an azure SQL database
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityLog>()
                .HasOne(a => a.User)
                .WithMany(u => u.ActivityLogs)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<JobUser>()
                .HasOne(a => a.User)
                .WithMany(u => u.JobUsers)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<JobLine>()
                .HasOne(a => a.Job)
                .WithMany(u => u.JobLines)
                .HasForeignKey(a => a.JobId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<EquipmentLog>()
                .HasOne(a => a.User)
                .WithMany(u => u.EquipmentLogs)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Comment>()
                .HasOne(a => a.Parent)
                .WithOne(b => b.Child)
                .HasForeignKey<Comment>(c => c.ParentId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>()
                .HasOne(a => a.Child)
                .WithOne(b => b.Parent)
                .HasForeignKey<Comment>(c => c.ChildId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasOne(c => c.PrimaryContact)
                .WithMany(u => u.ContactToCompanies)
                .HasForeignKey(c => c.PrimaryContactId);

            modelBuilder.Entity<Company>()
                .HasOne(c => c.CreatedBy)
                .WithMany(u => u.CreatedCompanies)
                .HasForeignKey(c => c.CreatedById);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Company)
                .WithMany(c => c.Users)
                .HasForeignKey(a => a.CompanyId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Line>()
                .HasOne(l => l.Area)
                .WithMany(a => a.Lines)
                .HasForeignKey(l => l.AreaId)
                .OnDelete(DeleteBehavior.NoAction);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Area> Areas { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyContact> CompanyContacts { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobLine> JobLines { get; set; }
        public DbSet<JobUser> JobUsers { get; set; }
        public DbSet<Line> Lines { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<Region> Regions { get; set; }
        public new DbSet<User> Users { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<EquipmentLog> EquipmentLogs { get; set; }
        public DbSet<MaintenanceLog> MaintenanceLogs { get; set; }
        public DbSet<MileageLog> MileageLogs { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<UserAudit> UserAuditEvents { get; set; }

        public DbSet<SpectrumFile> Files { get; set; }
        public DbSet<ConfirmationEmail> ConfirmationEmails { get; set; }
    }
}