﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inscyth.Spectrum.Data.Migrations
{
    public partial class DeriveUserAuditFromModelBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "UserAuditEvents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedById",
                table: "UserAuditEvents",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "UserAuditEvents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "UserAuditEvents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_UserAuditEvents_CreatedById",
                table: "UserAuditEvents",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAuditEvents_Users_CreatedById",
                table: "UserAuditEvents",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAuditEvents_Users_CreatedById",
                table: "UserAuditEvents");

            migrationBuilder.DropIndex(
                name: "IX_UserAuditEvents_CreatedById",
                table: "UserAuditEvents");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "UserAuditEvents");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "UserAuditEvents");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "UserAuditEvents");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "UserAuditEvents");
        }
    }
}
