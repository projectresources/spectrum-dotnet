﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inscyth.Spectrum.Data.Migrations
{
    public partial class RemoveAreaLines : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AreaLines");

            migrationBuilder.AddColumn<int>(
                name: "AreaId",
                table: "Lines",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Lines_AreaId",
                table: "Lines",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lines_Areas_AreaId",
                table: "Lines",
                column: "AreaId",
                principalTable: "Areas",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lines_Areas_AreaId",
                table: "Lines");

            migrationBuilder.DropIndex(
                name: "IX_Lines_AreaId",
                table: "Lines");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Lines");

            migrationBuilder.CreateTable(
                name: "AreaLines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AreaId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<int>(type: "int", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LineId = table.Column<int>(type: "int", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AreaLines_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AreaLines_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AreaLines_Lines_LineId",
                        column: x => x.LineId,
                        principalTable: "Lines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AreaLines_AreaId",
                table: "AreaLines",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaLines_CreatedById",
                table: "AreaLines",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_AreaLines_LineId",
                table: "AreaLines",
                column: "LineId");
        }
    }
}
