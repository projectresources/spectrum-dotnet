﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Inscyth.Spectrum.Data.Migrations
{
    public partial class CreateLocationColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "LocationEnd",
                table: "Regions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LocationStart",
                table: "Regions",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "End",
                table: "Lines",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Start",
                table: "Lines",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LocationEnd",
                table: "Areas",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LocationStart",
                table: "Areas",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationEnd",
                table: "Regions");

            migrationBuilder.DropColumn(
                name: "LocationStart",
                table: "Regions");

            migrationBuilder.DropColumn(
                name: "End",
                table: "Lines");

            migrationBuilder.DropColumn(
                name: "Start",
                table: "Lines");

            migrationBuilder.DropColumn(
                name: "LocationEnd",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "LocationStart",
                table: "Areas");
        }
    }
}
