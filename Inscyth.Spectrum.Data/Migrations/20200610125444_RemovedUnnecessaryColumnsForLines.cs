﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inscyth.Spectrum.Data.Migrations
{
    public partial class RemovedUnnecessaryColumnsForLines : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lines_Companies_CompanyId",
                table: "Lines");

            migrationBuilder.DropForeignKey(
                name: "FK_Lines_LineTypes_LineTypeId",
                table: "Lines");

            migrationBuilder.DropTable(
                name: "LineTypes");

            migrationBuilder.DropIndex(
                name: "IX_Lines_CompanyId",
                table: "Lines");

            migrationBuilder.DropIndex(
                name: "IX_Lines_LineTypeId",
                table: "Lines");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Lines");

            migrationBuilder.DropColumn(
                name: "LineTypeId",
                table: "Lines");

            migrationBuilder.AddColumn<string>(
                name: "LineType",
                table: "Lines",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LineType",
                table: "Lines");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "Lines",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LineTypeId",
                table: "Lines",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LineTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<int>(type: "int", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LineTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LineTypes_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lines_CompanyId",
                table: "Lines",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Lines_LineTypeId",
                table: "Lines",
                column: "LineTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_LineTypes_CreatedById",
                table: "LineTypes",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Lines_Companies_CompanyId",
                table: "Lines",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Lines_LineTypes_LineTypeId",
                table: "Lines",
                column: "LineTypeId",
                principalTable: "LineTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
