﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inscyth.Spectrum.Data.Migrations
{
    public partial class RemovedVehicleSourceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_VehicleSources_VehicleSourceId",
                table: "Vehicles");

            migrationBuilder.DropTable(
                name: "VehicleSources");

            migrationBuilder.DropIndex(
                name: "IX_Vehicles_VehicleSourceId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "VehicleSourceId",
                table: "Vehicles");

            migrationBuilder.AddColumn<int>(
                name: "DoeId",
                table: "Vehicles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "VehicleMake",
                table: "Vehicles",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "VehicleModel",
                table: "Vehicles",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "VehicleYear",
                table: "Vehicles",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DoeId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "VehicleMake",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "VehicleModel",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "VehicleYear",
                table: "Vehicles");

            migrationBuilder.AddColumn<int>(
                name: "VehicleSourceId",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "VehicleSources",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<int>(type: "int", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DoeId = table.Column<int>(type: "int", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    VehicleMake = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    VehicleModel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    VehicleYear = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleSources_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_VehicleSourceId",
                table: "Vehicles",
                column: "VehicleSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSources_CreatedById",
                table: "VehicleSources",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_VehicleSources_VehicleSourceId",
                table: "Vehicles",
                column: "VehicleSourceId",
                principalTable: "VehicleSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
