using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Xunit;

namespace Inscyth.Spectrum.Services.Tests
{
    
    public class GenericRepositoryTests
    {
        [Fact]
        public async Task Insert_NewCompany_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("insert_company");

            // Arrange
            var obj = new Company(name: "Fake Name")
            {
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);

                // Act
                await repo.InsertAsync(obj);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Single(context.Companies, h => h.Id == obj.Id);
            }
        }

        [Fact]
        public async Task InsertRange_NewCompanies_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("insertrange_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                },
                new Company(name: "Fake Name2")
                {
                }
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);

                // Act
                await repo.InsertRangeAsync(objs);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Equal(2, context.Companies.Count());
            }
        }


        [Fact]
        public async Task UpdateAsync_NewCompany_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("update_company");

            // Arrange
            var obj = new Company(name: "Fake Name")
            {
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await repo.InsertAsync(obj);
            }

            // Act
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                obj.Name = "Updated Name";
                await repo.Update(obj);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var fetch = context.Companies.Single(h => h.Id == obj.Id);
                Assert.Equal("Updated Name", fetch.Name);
            }
        }


        [Fact]
        public async Task DeleteAsync_Company_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("delete_company");

            // Arrange
            var obj = new Company(name: "Fake Name")
            {
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await repo.InsertAsync(obj);
            }

            // Act
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await repo.Delete(obj);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var fetch = context.Companies.SingleOrDefault(h => h.Id == obj.Id);
                Assert.Null(fetch);
            }
        }

        [Fact]
        public async Task DeleteByIdAsync_Company_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("deletebyid_company");

            // Arrange
            var obj = new Company(name: "Fake Name")
            {
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await repo.InsertAsync(obj);
            }

            // Act
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await repo.DeleteById(obj.Id);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var fetch = context.Companies.SingleOrDefault(h => h.Id == obj.Id);
                Assert.Null(fetch);
            }
        }

        [Fact]
        public async Task GetAllAsync_Companies_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("getall_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                },
                new Company(name: "Fake Name 2")
                {
                },
            };

            List<Company> allHps;
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await context.AddRangeAsync(objs);
                await context.SaveChangesAsync();

                // Act
                allHps = repo.GetAll().ToList();
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Collection(allHps, hp => Assert.True(context.Companies.Contains(hp)),
                    hp => Assert.True(context.Companies.Contains(hp)));
            }
        }

        [Fact]
        public async Task GetByIdAsync_Company_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("getbyid_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name"),
                new Company(name: "Fake Name2"),
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                await context.AddRangeAsync(objs);
                await context.SaveChangesAsync();
            }

            Company hp;
            // Act
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                hp = await repo.GetByIdAsync(objs.First().Id);
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Equal(JsonConvert.SerializeObject(hp), JsonConvert.SerializeObject(objs.First()));
            }
        }


        [Fact]
        public async Task GetWhereAsync_Company_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("getwhere_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name"),
                new Company(name: "Fake Name2"),
            };

            await using (var context = new SpectrumDbContext(dbOptions))
            {
                await context.AddRangeAsync(objs);
                await context.SaveChangesAsync();
            }

            List<Company> hps;
            // Act
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                hps = await repo.GetWhere(o => o.Name.StartsWith("Fake Name")).ToListAsync();
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Collection(hps,
                    hp => Assert.Contains(objs, o => JsonConvert.SerializeObject(o) == JsonConvert.SerializeObject(hp)),
                    hp => Assert.Contains(objs,
                        o => JsonConvert.SerializeObject(o) == JsonConvert.SerializeObject(hp)));
            }
        }

        [Fact]
        public async Task CountAllAsync_Companies_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("countall_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                },
                new Company(name: "Fake Name 2")
                {
                }
            };

            int count = 0;
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);

                // Act
                count = await repo.CountAllAsync();
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Equal(context.Companies.Count(), count);
            }
        }


        [Fact]
        public async Task CountWhereAsync_Companies_Success()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>("countwhere_company");

            // Arrange
            var objs = new List<Company>
            {
                new Company(name: "Fake Name")
                {
                },
                new Company(name: "Fake Name 2")
                {
                }
            };

            int count = 0;
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var repo = new GenericRepository<Company, SpectrumDbContext>(context);
                await context.AddRangeAsync(objs);
                await context.SaveChangesAsync();
                // Act
                count = await repo.CountWhereAsync(hp => hp.Name == "Fake Name");
            }

            // Assert
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                Assert.Equal(1, count);
            }
        }
    }
}