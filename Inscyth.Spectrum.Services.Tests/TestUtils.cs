﻿using Microsoft.EntityFrameworkCore;

namespace Inscyth.Spectrum.Services.Tests
{
    public static class TestUtils
    {
        public static DbContextOptions<T> GetInMemoryDbOptions<T>(string databaseName)
            where T : DbContext
        {
            var options = new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;
            
            return options;
        }
    }
}