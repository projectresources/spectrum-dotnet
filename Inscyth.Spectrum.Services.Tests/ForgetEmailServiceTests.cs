﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inscyth.Spectrum.Data;
using Inscyth.Spectrum.Data.Entities;
using Inscyth.Spectrum.Data.Repository;
using Inscyth.Spectrum.Services.Email;
using Inscyth.Spectrum.Services.Interfaces;
using Inscyth.Spectrum.Services.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace Inscyth.Spectrum.Services.Tests
{
    public class ForgetEmailServiceTests
    {
        [Fact]
        public void SendForgetEmail_Success_ShouldReturnTrue()
        {
        }

        [Fact]
        public void SendForgetEmail_Failure_ShouldReturnException()
        {
        }

        [Fact]
        public async Task ValidateGuid_Success_ShouldReturnTrue()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>(Guid.NewGuid().ToString());
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var emails = new List<ConfirmationEmail>()
                {
                    new ConfirmationEmail("andrei@gmail.com", "1234-1234-1234-1234",
                        ConfirmationEmailType.Confirmation),
                    new ConfirmationEmail("andrei@gmail.com", "1234-1234-1234-1234",
                        ConfirmationEmailType.ForgotPassword),
                    new ConfirmationEmail("Andrei@yahoo.com", "43214321123123", ConfirmationEmailType.Confirmation)
                };

                var repo = new GenericRepository<ConfirmationEmail, SpectrumDbContext>(context);
                await context.AddRangeAsync(emails);
                await context.SaveChangesAsync();
            }

            await using var assertContext = new SpectrumDbContext(dbOptions);
            var emailService = new Mock<IEmailService>();
            var emailFactory = new Mock<IEmailMessageFactory>();
            var forgetEmailService = new ChangePasswordService(emailService.Object, emailFactory.Object, assertContext);

            var result =
                await forgetEmailService.ValidateGuid(emailAddress: "andrei@gmail.com",
                    guid: "1234-1234-1234-1234");

            result.Should().BeTrue();

            var confirmationEmail = await assertContext.ConfirmationEmails.FirstOrDefaultAsync(e =>
                e.EmailAddress == "andrei@gmail.com" && e.Guid == "1234-1234-1234-1234" &&
                e.Type == ConfirmationEmailType.ForgotPassword);

            confirmationEmail.Should().NotBeNull();
            confirmationEmail.As<ConfirmationEmail>().IsActive.Should().BeFalse();
        }

        [Fact]
        public async Task ValidateGuid_Failure_ShouldReturnFalse()
        {
            var dbOptions = TestUtils.GetInMemoryDbOptions<SpectrumDbContext>(Guid.NewGuid().ToString());
            await using (var context = new SpectrumDbContext(dbOptions))
            {
                var emails = new List<ConfirmationEmail>()
                {
                    new ConfirmationEmail("andrei@gmail.com", "1234-1234-1234-1234",
                        ConfirmationEmailType.Confirmation),
                    new ConfirmationEmail("Andrei@yahoo.com", "43214321123123", ConfirmationEmailType.Confirmation)
                };

                var repo = new GenericRepository<ConfirmationEmail, SpectrumDbContext>(context);
                await context.AddRangeAsync(emails);
                await context.SaveChangesAsync();
            }

            await using var assertContext = new SpectrumDbContext(dbOptions);
            var emailService = new Mock<IEmailService>();
            var emailFactory = new Mock<IEmailMessageFactory>();
            var forgetEmailService = new ChangePasswordService(emailService.Object, emailFactory.Object, assertContext);

            var result =
                await forgetEmailService.ValidateGuid(emailAddress: "andrei@gmail.com",
                    guid: "1234-1234-1234-1234");

            result.Should().BeFalse();
        }
    }
}